import csv
import mcp9600
from RPi import GPIO
import time

tc = mcp9600.MCP9600(i2c_addr=0x67)

T1 = 100
T2 = 200

GPIO_RELAY = 16

# Set up
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(GPIO_RELAY, GPIO.OUT)
GPIO.output(GPIO_RELAY, False)

print("Checking that oven is cold...")
while(tc.get_hot_junction_temperature() > 30):
    print(f"Temperature: {tc.get_hot_junction_temperature()}")
    time.sleep(1)

with open('calibration_curve.csv', 'w') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=['Time','Temp','Oven_State'])
    writer.writeheader()
    row={}

    print("Turning oven on...")
    GPIO.output(GPIO_RELAY, True)

    for i in range(0,10):
        row['Time'] = time.monotonic()
        row['Temp'] = tc.get_hot_junction_temperature()
        row['Oven_State'] = GPIO.input(GPIO_RELAY)
        writer.writerow(row)
        time.sleep(1)

    print("Beginning initial ramp rate characterization")
    t_ramp_initial = time.monotonic()
    ramp_initial = tc.get_hot_junction_temperature()

    for i in range(0,30):
        row['Time'] = time.monotonic()
        row['Temp'] = tc.get_hot_junction_temperature()
        row['Oven_State'] = GPIO.input(GPIO_RELAY)
        writer.writerow(row)
        time.sleep(1)

    ramp_final = tc.get_hot_junction_temperature()
    t_ramp_final = time.monotonic()

    t_ramp_delta = t_ramp_final - t_ramp_initial
    ramp_delta = ramp_final - ramp_initial

    ramp_rate = ramp_delta / t_ramp_delta

    print(f'Rising Temp Ramp Rate: {ramp_rate} C')
    print(f"Current Temp: {row['Temp']}")
    while(row['Temp'] < T1):
        row['Time'] = time.monotonic()
        row['Temp'] = tc.get_hot_junction_temperature()
        row['Oven_State'] = GPIO.input(GPIO_RELAY)
        writer.writerow(row)
        time.sleep(1)
    
    print("Checking lag time")
    GPIO.output(GPIO_RELAY, False)
    row['Time'] = time.monotonic()
    t_lag_initial = row['Time']
    row['Temp'] = tc.get_hot_junction_temperature()
    lag_initial = row['Temp']
    row['Oven_State'] = GPIO.input(GPIO_RELAY)
    writer.writerow(row)

    t_lag_final = 0
    lag_final = 0
    while(row['Temp'] >= T1):
        time.sleep(1)
        row['Time'] = time.monotonic()
        t_lag_final = row['Time']
        row['Temp'] = tc.get_hot_junction_temperature()
        lag_final = row['Temp']
        row['Oven_State'] = GPIO.input(GPIO_RELAY)
        writer.writerow(row)
    
    print("Beginning falling ramp rate characterization")
    t_ramp_initial = time.monotonic()
    ramp_initial = tc.get_hot_junction_temperature()

    for i in range(0,30):
        row['Time'] = time.monotonic()
        row['Temp'] = tc.get_hot_junction_temperature()
        row['Oven_State'] = GPIO.input(GPIO_RELAY)
        writer.writerow(row)
        time.sleep(1)

    ramp_final = tc.get_hot_junction_temperature()
    t_ramp_final = time.monotonic()

    t_ramp_delta = t_ramp_final - t_ramp_initial
    ramp_delta = ramp_final - ramp_initial

    ramp_rate = ramp_delta / t_ramp_delta

    print(f'Falling Temp Ramp Rate: {ramp_rate} C')