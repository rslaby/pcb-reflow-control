from RPi import GPIO
import time

# GPIO to BCM Pin Mapping
LCD_RS = 17
LCD_RW = 18
LCD_E  = 19
LCD_D0 = 20
LCD_D1 = 21
LCD_D2 = 22
LCD_D3 = 23
LCD_D4 = 24
LCD_D5 = 25
LCD_D6 = 26
LCD_D7 = 27

LCD_D0_MASK = 0b00000001
LCD_D1_MASK = 0b00000010
LCD_D2_MASK = 0b00000100
LCD_D3_MASK = 0b00001000
LCD_D4_MASK = 0b00010000
LCD_D5_MASK = 0b00100000
LCD_D6_MASK = 0b01000000
LCD_D7_MASK = 0b10000000


# This function sends a Character to the specified cursor position on 
# the LCD Display.
# Params: in, in
def LCDChar(text, cursorPosition):
    LCDCommand(cursorPosition)
    time.sleep(0.01)
    LCDData(text)


# This function sends a Register command to the LCD Display.
# Params: in
def LCDCommand(command):
    GPIO.output(LCD_D0, command & LCD_D0_MASK)
    GPIO.output(LCD_D1, command & LCD_D1_MASK)
    GPIO.output(LCD_D2, command & LCD_D2_MASK)
    GPIO.output(LCD_D3, command & LCD_D3_MASK)
    GPIO.output(LCD_D4, command & LCD_D4_MASK)
    GPIO.output(LCD_D5, command & LCD_D5_MASK)
    GPIO.output(LCD_D6, command & LCD_D6_MASK)
    GPIO.output(LCD_D7, command & LCD_D7_MASK)

    GPIO.output(LCD_RS, False)
    GPIO.output(LCD_RW, False)
    time.sleep(0.01)
    GPIO.output(LCD_E, True)
    time.sleep(0.01)
    GPIO.output(LCD_E, False)
    time.sleep(0.01)

# This function sends a character to the display.
# Params: in
def LCDData(data):
    GPIO.output(LCD_D0, data & LCD_D0_MASK)
    GPIO.output(LCD_D1, data & LCD_D1_MASK)
    GPIO.output(LCD_D2, data & LCD_D2_MASK)
    GPIO.output(LCD_D3, data & LCD_D3_MASK)
    GPIO.output(LCD_D4, data & LCD_D4_MASK)
    GPIO.output(LCD_D5, data & LCD_D5_MASK)
    GPIO.output(LCD_D6, data & LCD_D6_MASK)
    GPIO.output(LCD_D7, data & LCD_D7_MASK)
    
    GPIO.output(LCD_RW, False)
    GPIO.output(LCD_RS, True)
    GPIO.output(LCD_E, True)
    time.sleep(0.01)
    GPIO.output(LCD_RS, False)
    GPIO.output(LCD_E, False)
    time.sleep(0.01)


# This function initializes the LCD Display.
# Params: none
def LCDInit():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(LCD_E,GPIO.OUT)
    GPIO.setup(LCD_RS,GPIO.OUT)
    GPIO.setup(LCD_RW,GPIO.OUT)
    GPIO.setup(LCD_D7,GPIO.OUT)
    GPIO.setup(LCD_D6,GPIO.OUT)
    GPIO.setup(LCD_D5,GPIO.OUT)
    GPIO.setup(LCD_D4,GPIO.OUT)
    GPIO.setup(LCD_D3,GPIO.OUT)
    GPIO.setup(LCD_D2,GPIO.OUT)
    GPIO.setup(LCD_D1,GPIO.OUT)
    GPIO.setup(LCD_D0,GPIO.OUT)
    time.sleep(0.05)
    # Function Set
    LCDCommand(0x3C)
    # Function Set
    LCDCommand(0x3C)
    # Display On
    LCDCommand(0x0F)
    # Clear Display
    LCDCommand(0x01)
    time.sleep(0.01)
    # Set Entry Mode
    LCDCommand(0x06)

# This function sends a string value to the specified cursor position on 
# the LCD display.
# Params: ref, in
def LCDString(text, cursorPosition):
    LCDCommand(cursorPosition)
    for c in text:
        LCDData(c)