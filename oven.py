import csv
import mcp9600
from RPi import GPIO
import time

tc = mcp9600.MCP9600(i2c_addr=0x67)

GPIO_RELAY = 16

# Temp to start heating
TEMP_MAX_START = 50
# Temp during preheat
TEMP_PREHEAT = 150
# Reflow target temp
TEMP_REFLOW = 215

# How long to soak
TIME_SOAK = 120

# Overshoot in C
OVEN_OVERSHOOT = 20
# Time to max temp after shutdown
OVEN_LAG = 112

# Set up
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(GPIO_RELAY, GPIO.OUT)
GPIO.output(GPIO_RELAY, False)

print("Checking that oven is cold...")
while(tc.get_hot_junction_temperature() > TEMP_MAX_START):
    print(f"Temperature: {tc.get_hot_junction_temperature()}")
    time.sleep(1)


with open('oven_last_curve.csv', 'w') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=['Time','Temp','Oven_State'])
    writer.writeheader()
    row={}

    print("Ramping to preheat temp...")
    GPIO.output(GPIO_RELAY, True)
    row['Temp'] = tc.get_hot_junction_temperature()
    while(row['Temp'] < TEMP_PREHEAT - OVEN_OVERSHOOT):
        row['Time'] = time.monotonic()
        row['Temp'] = tc.get_hot_junction_temperature()
        row['Oven_State'] = GPIO.input(GPIO_RELAY)
        writer.writerow(row)
        print(f"Current Temp: {row['Temp']}")
        time.sleep(1)

    GPIO.output(GPIO_RELAY, False)
    print("Waiting to reach soak temp...")
    while(row['Temp'] < TEMP_PREHEAT - OVEN_OVERSHOOT):
        row['Time'] = time.monotonic()
        row['Temp'] = tc.get_hot_junction_temperature()
        row['Oven_State'] = GPIO.input(GPIO_RELAY)
        writer.writerow(row)
        print(f"Current Temp: {row['Temp']}")
        time.sleep(1)
    
    print("Entering soak phase...")
    soak_start = time.monotonic()

    while(time.monotonic() < soak_start + TIME_SOAK):
        row['Time'] = time.monotonic()
        row['Temp'] = tc.get_hot_junction_temperature()
        row['Oven_State'] = GPIO.input(GPIO_RELAY)
        writer.writerow(row)
        print(f"Current Temp: {row['Temp']}")
        time.sleep(1)

    GPIO.output(GPIO_RELAY, True)
    print("Ramping to reflow temp...")
    while(row['Temp'] < TEMP_REFLOW - OVEN_OVERSHOOT):
        row['Time'] = time.monotonic()
        row['Temp'] = tc.get_hot_junction_temperature()
        row['Oven_State'] = GPIO.input(GPIO_RELAY)
        writer.writerow(row)
        print(f"Current Temp: {row['Temp']}")
        time.sleep(1)
    
    GPIO.output(GPIO_RELAY, False)
    print("Turning off oven...")
    while(row['Temp'] > TEMP_MAX_START):
        row['Time'] = time.monotonic()
        row['Temp'] = tc.get_hot_junction_temperature()
        row['Oven_State'] = GPIO.input(GPIO_RELAY)
        writer.writerow(row)
        print(f"Current Temp: {row['Temp']}")
        time.sleep(1)