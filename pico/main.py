from machine import I2C, Pin
import i2c_lcd
import mcp9600
import time

OVEN_RELAY = 22
LCD_ADDR = 0x27
MCP9600_ADDR = 0x67

i2c0 = I2C(0)
i2c1 = I2C(1, sda=Pin(2), scl=Pin(3), freq=100_000)

oven_relay = Pin(OVEN_RELAY,Pin.OUT)

# Temp to start heating
TEMP_MAX_START = 50
# Temp during preheat
TEMP_PREHEAT = 150
# Reflow target temp
TEMP_REFLOW = 215
# How long to soak
TIME_SOAK = 120
# Overshoot in C
OVEN_OVERSHOOT = 20
# Time to max temp after shutdown
OVEN_LAG = 112


class Display:
    def __init__(self,bus,status,temp,time):
        self.bus = bus
        self.status = status
        self.temp = temp
        self.time = time
        self.lcd = i2c_lcd.I2cLcd(i2c0,LCD_ADDR,2,16)
        self.update_display()
    
    def update_display(self):
        self.lcd.clear()
        self.lcd.move_to(0,0)
        self.lcd.putstr(f"{self.status}")
        self.lcd.move_to(0,1)
        self.lcd.putstr(f"{self.temp}C ")
        self.lcd.move_to(10,1)
        self.lcd.putstr(f"{self.time}s")


class Oven():
    def __init__(self,pwrctl,tc_bus,tc_addr):
        self.pwrctl = pwrctl
        self.tc_bus = tc_bus
        self.tc_addr = tc_addr
        self.tc = mcp9600.MCP9600(self.tc_bus,self.tc_addr)
        #self.tc.set_type("K")

    def on(self):
        self.pwrctl.high()

    def off(self):
        self.pwrctl.low()
    
    def get_temp(self):
        return self.tc.read_th_temp()



def main():
    start_time = time.time()
    oven = Oven(pwrctl=oven_relay, tc_bus=i2c1, tc_addr=MCP9600_ADDR)
    display = Display(i2c0,"Starting...",oven.get_temp(),start_time)
    while display.temp >= 50:
        display.status = "Too Hot"
        display.temp = oven.get_temp()
        display.time = time.time() - start_time
        display.update_display()
        time.sleep(1)
    oven.on()
    display.status = "Heating"
    while(display.temp < TEMP_PREHEAT - OVEN_OVERSHOOT):
        display.temp = oven.get_temp()
        display.time = time.time() - start_time
        display.update_display()
        time.sleep(1)
    oven.off()
    display.status = "Soaking"
    soak_start = time.time()
    while(display.time < soak_start + TIME_SOAK):
        display.temp = oven.get_temp()
        display.time = time.time() - start_time
        display.update_display()
        time.sleep(1)
    oven.on()
    display.status = "Ramp to Reflow"
    while(display.temp < TEMP_REFLOW - OVEN_OVERSHOOT):
        display.temp = oven.get_temp()
        display.time = time.time() - start_time
        display.update_display()
        time.sleep(1)  
    oven.off()  
    while display.temp >= 50:
        display.status = "Cooling"
        display.temp = oven.get_temp()
        display.time = time.time() - start_time
        display.update_display()
        time.sleep(1)


if __name__ == "__main__":
    main()